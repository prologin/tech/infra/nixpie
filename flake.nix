{
  description = "Collection of Nix packages, NixOS modules and configurations used by Prologin.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nixpkgs-master.url = "github:NixOS/nixpkgs/master";

    nixpie = {
      url = "git+https://gitlab.cri.epita.fr/cri/infrastructure/nixpie.git";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        nixpkgsUnstable.follows = "nixpkgs-unstable";
        nixpkgsMaster.follows = "nixpkgs-master";
        docker-nixpkgs.follows = "docker-nixpkgs";
      };
    };

    docker-nixpkgs = {
      url = "github:nix-community/docker-nixpkgs";
      flake = false;
    };

    prolowalls.url = "git+https://gitlab.com/prologin/asso/wallpapers?ref=cube";

    alejandra.url = "github:kamadorueda/alejandra/3.0.0";
    flake-utils-plus.url = "github:gytis-ivaskevicius/flake-utils-plus/v1.3.1";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils-plus,
    ...
  } @ inputs: let
    inherit (nixpkgs) lib;
    inherit (lib) optionalAttrs recursiveUpdate singleton;
    inherit (flake-utils-plus.lib) mkApp mkFlake;
  in
    mkFlake rec {
      inherit self inputs;

      supportedSystems = ["x86_64-linux"];

      channelsConfig = {
        allowUnfree = true;
      };
      sharedOverlays = [
        (import "${inputs.docker-nixpkgs}/overlay.nix")
        inputs.alejandra.overlay
        inputs.nixpie.overlays.clonezilla
        inputs.nixpie.overlays.exec-tools
        inputs.nixpie.overlays.nixpie-utils
        inputs.nixpie.overlays.pam_afs_session
        inputs.nixpie.overlays.sddm-epita-themes
        inputs.nixpie.overlays.term_size
        (import ./overlays/xfdesktop.nix {inherit inputs;})
        (import ./overlays/epita-themes-sddm.nix {inherit inputs;})
      ];

      channels = {
        nixpkgs = {
          input = inputs.nixpkgs;
          overlaysBuilder = channels: [
            (final: prev: {
              inherit
                (channels.nixpkgs-unstable)
                awscli
                nix-diff
                shellcheck
                ;
            })
          ];
        };
        nixpkgs-unstable = {
          input = inputs.nixpkgs-unstable;
        };
        nixpkgs-master = {
          input = inputs.nixpkgs-master;
        };
      };

      nixosModules =
        (import ./modules)
        // {
          profiles = import ./profiles;
          nixpie = import ./modules/nixpie.nix;
        };

      hostDefaults = {
        system = "x86_64-linux";
        modules = [];
        channelName = "nixpkgs";
      };
      hosts = import ./images inputs;

      outputsBuilder = channels: {
        checks = {};

        packages = {} // (import ./images/docker.nix inputs channels);

        devShell = channels.nixpkgs.mkShell {
          buildInputs = with channels.nixpkgs; [
            alejandra
            awscli
            git
            nix-diff
            pre-commit
            shellcheck
          ];
        };

        apps = let
          isStandardImage = name: !lib.any (suffix: lib.hasSuffix suffix name) ["-vm" "-iso" "-epita" "-ova"];
          checkList = builtins.attrNames self.checks.${channels.nixpkgs.system};
          imageList = builtins.attrNames (lib.filterAttrs (name: _: isStandardImage name) self.nixosConfigurations);
          pkgsList = builtins.attrNames (lib.filterAttrs (name: _: !lib.hasSuffix "-docker" name) self.packages.${channels.nixpkgs.system});
          dockerList = builtins.attrNames (lib.filterAttrs (name: _: lib.hasSuffix "-docker" name) self.packages.${channels.nixpkgs.system});
          mkListApp = list:
            mkApp {
              drv = channels.nixpkgs.writeShellScriptBin "list.sh" (lib.concatMapStringsSep "\n" (el: "echo '${el}'") list);
            };
        in {
          list-checks = mkListApp checkList;
          list-docker = mkListApp dockerList;
          list-images = mkListApp imageList;
          list-pkgs = mkListApp pkgsList;

          alejandra = mkApp {
            drv = channels.nixpkgs.alejandra;
          };
          awscli = mkApp {
            drv = channels.nixpkgs.awscli;
            exePath = "/bin/aws";
          };
          nix-diff = mkApp {
            drv = channels.nixpkgs.nix-diff;
          };
          skopeo = mkApp {
            drv = channels.nixpkgs.skopeo;
          };
        };
      };
    };
}
