{inputs}: final: prev: {
  xfce =
    prev.xfce
    // {
      xfdesktop = prev.xfce.xfdesktop.overrideAttrs (oldAttrs: {
        postInstall = ''
          cp ${inputs.prolowalls}/wallpapers/prolo2023.png $out/share/backgrounds/xfce/xfce-verticals.png
        '';
      });
    };
}
