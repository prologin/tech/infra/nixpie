{inputs}: final: prev: {
  sddm-epita-themes = prev.sddm-epita-themes.overrideAttrs (oldAttrs: {
    installPhase =
      oldAttrs.installPhase
      + ''
        cp ${inputs.prolowalls}/logo/cube.png $out/share/sddm/themes/epita-simplyblack/epita.png
      '';
  });
}
