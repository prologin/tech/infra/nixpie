{
  pkgs,
  config,
  lib,
  modulesPath,
  ...
}: {
  # Make sure we can boot everywhere
  imports = [
    "${modulesPath}/profiles/all-hardware.nix"
  ];
  boot.initrd.availableKernelModules = ["r8169"];

  fileSystems = lib.mkDefault {
    "/" = {
      fsType = "ext4";
      label = "nixos-root";
    };
  };

  boot.loader.grub.device = lib.mkDefault "nodev";

  i18n.defaultLocale = "en_US.UTF-8";
  time.timeZone = "Europe/Paris";

  console = {
    earlySetup = true;
    keyMap = "us";
  };

  nix = {
    package = pkgs.nixFlakes;
    settings = {
      system-features = ["nixos-test" "benchmark" "big-parallel" "kvm"];

      auto-optimise-store = true;

      sandbox = true;

      trusted-users = ["root" "@wheel"];

      substituters = [
        "https://nix-cache.s3.prologin.org/"
        "https://s3.cri.epita.fr/cri-nix-cache.s3.cri.epita.fr/"
      ];
      trusted-public-keys = [
        "cache.nix.prologin.org:OZMs46jaN4mLZ3Tbb1oFC4JRuyNYZy29I+JoDKjLrh0="
        "cache.nix.cri.epita.fr:qDIfJpZWGBWaGXKO3wZL1zmC+DikhMwFRO4RVE6VVeo="
      ];
    };

    gc.automatic = false;
    optimise.automatic = false;

    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  networking = {
    useDHCP = true;
    dhcpcd = {
      wait = "any"; # make sure we get an IP before marking the service as up
      extraConfig = ''
        noipv4ll
      '';
    };
    nameservers = ["1.1.1.1" "1.0.0.1"];
    # TODO: add VPN ntp
    timeServers = [
      "ntp.pie.cri.epita.fr"
      "0.nixos.pool.ntp.org"
      "1.nixos.pool.ntp.org"
      "2.nixos.pool.ntp.org"
      "3.nixos.pool.ntp.org"
    ];
  };

  security = {
    protectKernelImage = true;
  };

  hardware.enableRedistributableFirmware = true;

  netboot = {
    torrent = {
      announceURL = "http://torrent.pie.prologin.org:8000/announce";
      webseed.url = "https://pxe-images.s3.prologin.org/";
    };
    fallbackNameservers = ["1.1.1.1" "1.0.0.1"];
  };

  cri = {
    aria2.enable = true;
    node-exporter.enable = true;
    salt = {
      enable = true;
      master = "salt.pie.prologin.org";
    };
    sshd.enable = true;
    yubikey.enable = true;
  };

  prologin = {
    tailscale.enable = true;
    users.enable = true;
  };

  programs.vim = {
    defaultEditor = true;
    package = pkgs.vim_configurable;
  };

  cri.packages = {
    pkgs = {
      core.enable = true;
      fuse.enable = true;
    };

    python = {
      core.enable = true;
    };
  };

  documentation = {
    enable = true;
    dev.enable = true;
    doc.enable = true;
    info.enable = true;
    man = {
      enable = true;
      generateCaches = true;
    };
    nixos.enable = true;
  };

  # HACK: this is needed to be able to compile with external libs
  environment = {
    pathsToLink = ["/include" "/lib"];
    extraOutputsToInstall = ["out" "lib" "bin" "dev"];
    variables = {
      NIXPKGS_ALLOW_UNFREE = "1";

      NIX_CFLAGS_COMPILE_x86_64_unknown_linux_gnu = "-I/run/current-system/sw/include";
      NIX_CFLAGS_LINK_x86_64_unknown_linux_gnu = "-L/run/current-system/sw/lib";

      IDEA_JDK = "/run/current-system/sw/lib/openjdk/";
      PKG_CONFIG_PATH = "/run/current-system/sw/lib/pkgconfig";
    };
  };

  programs.ssh = {
    startAgent = true;
    extraConfig = ''
      AddKeysToAgent yes
    '';
  };

  programs.gnupg = {
    dirmngr.enable = true;
    agent = {
      enable = true;
      pinentryFlavor = "gtk2";
      enableBrowserSocket = true;
      enableExtraSocket = true;
      enableSSHSupport = false;
    };
  };

  programs.udevil.enable = true;

  services.lldpd.enable = true;

  system.stateVersion = "22.11";
}
