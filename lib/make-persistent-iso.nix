{
  e2fsprogs,
  isoImage,
  partLabel ? "rwhome",
  runCommand,
  util-linux,
  vmTools,
  writableSpace ? 2048, # in MiB
}: let
  additionalSectors = writableSpace * 1024 * 2; # a sector is 512 bytes
in
  vmTools.runInLinuxVM (
    runCommand "persistent-iso" {
      preVM = ''
        # get 'standard' live ISO to add the writeable partition
        ISO_NAME=$(basename ${isoImage}/iso/*.iso)
        cp ${isoImage}/iso/$ISO_NAME $ISO_NAME
        chmod +w $ISO_NAME

        # add blank bytes to the ISO, this is where we'll create the new partition
        dd status=progress if=/dev/zero bs=512 count=${toString additionalSectors} >> $ISO_NAME

        # create a new primary partition, with default size (the whitespace is here for this)
        # NOTE: the --wipe=never flag prevents fdisk from erasing the iso9660 signature
        ${util-linux}/bin/fdisk --wipe=never -t dos $ISO_NAME <<EOF
        n
        p



        w
        EOF

        # runInLinuxVM can bind diskImage to /dev/vda automatically
        diskImage=$ISO_NAME
      '';
      postVM = ''
        mkdir $out/iso/
        mv $ISO_NAME $out/iso/
      '';
    }
    ''
      # now we need to create the ext4 filesystem in the new partition, this is
      # only doable in a block device, so we need to run inside a VM to mount
      # the ISO as a block device

      ${e2fsprogs}/bin/mkfs.ext4 -L ${partLabel} /dev/vda3
    ''
  )
