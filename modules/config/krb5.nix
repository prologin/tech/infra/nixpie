{
  config,
  lib,
  ...
}:
with lib; {
  options = {
    prologin.krb5 = {
      enable = mkEnableOption "Whether to enable Kerberos authentication.";
    };
  };

  config = mkIf config.prologin.krb5.enable {
    krb5 = {
      enable = true;
      libdefaults = {
        default_realm = "PROLOGIN.ORG";
        dns_fallback = true;
        dns_canonicalize_hostname = false;
        rnds = false;
        forwardable = true;
      };

      realms = {
        "PROLOGIN.ORG" = {
          admin_server = "kerberos.prologin.org";
        };
      };
    };
  };
}
