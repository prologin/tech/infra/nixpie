{
  config,
  lib,
  ...
}:
with lib; {
  options = {
    prologin.ldap = {
      enable = mkEnableOption "Enable default users";
    };
  };

  config = mkIf config.prologin.ldap.enable {
    services.sssd = {
      enable = true;
      config = ''
        [sssd]
        config_file_version = 2
        services = nss, pam, ssh
        domains = prologin.org

        [nss]
        override_shell = ${config.users.defaultUserShell}/bin/bash
        homedir_substring = /afs/prologin.org/user
        override_homedir = %H/%o

        [domain/prologin.org]
        cache_credentials = true
        enumerate = false

        id_provider = ldap
        auth_provider = ldap

        ldap_uri = ldaps://auth.pie.prologin.org
        ldap_search_base = dc=prologin,dc=org
        ldap_user_search_base = cn=users,cn=accounts,dc=prologin,dc=org?subtree?(objectClass=posixAccount)
        ldap_group_search_base = cn=groups,cn=accounts,dc=prologin,dc=org?subtree?(objectClass=posixGroup)
        ldap_id_use_start_tls = true
        ldap_tls_reqcert = allow
        ldap_schema = IPA

        entry_cache_timeout = 600
        ldap_network_timeout = 2
      '';
    };

    # TODO: this may not be needed
    users = {
      ldap = {
        enable = true;
        base = "dc=prologin,dc=org";
        server = "ldap://auth.pie.prologin.org";
        nsswitch = false;
      };
    };

    systemd.services.sssd = {
      requires = optional config.services.tailscale.enable "tailscale-login.service";
      after = optional config.services.tailscale.enable "tailscale-login.service";
      serviceConfig = {
        Restart = "always";
      };
    };
  };
}
