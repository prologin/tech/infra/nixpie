{
  config,
  lib,
  ...
}: {
  options = {
    prologin.firewall = {
      enable = lib.mkEnableOption "Semi-finals firewall";
    };
  };

  config = lib.mkIf config.prologin.firewall.enable {
    networking.firewall.enable = false;

    networking.nftables = {
      enable = true;
      ruleset = ''
        table inet filter {
          # Block all incomming connections traffic except SSH and "ping".
          chain input {
            type filter hook input priority 0;

            # accept any localhost traffic
            iifname lo accept

            # accept traffic originated from us
            ct state {established, related} accept

            ${lib.concatMapStringsSep "\n" (port: "tcp dport ${toString port} accept") config.networking.firewall.allowedTCPPorts}
            ${lib.concatMapStringsSep "\n" (port: "udp dport ${toString port} accept") config.networking.firewall.allowedUDPPorts}

            # Allow Prometheus exporters
            tcp dport {9100} accept

            # Allow ICMP
            ip protocol icmp accept

            drop
          }

          # Allow all outgoing connections.
          chain output {
            type filter hook output priority 0;

            # accept any localhost traffic
            iifname lo accept
            ip daddr 127.0.0.0/8 accept
            ip daddr 10.0.0.0/8 accept
            ip daddr 172.16.0.0/12 accept
            ip daddr 100.64.0.0/10 accept

            # accept traffic originated from us
            ct state {established, related} accept

            # DHCP
            ip daddr gate.pie.cri.epita.fr accept

            # ER Server
            ip daddr iris.th2.hxg.prologin.dev accept

            # Java Stuff
            # repo1.maven.org
            ip daddr {199.232.192.209,199.232.196.209} accept
            # repo.maven.apache.org
            ip daddr {199.232.192.215,199.232.196.215} accept
            # services.gradle.org
            ip daddr {104.18.190.9,104.18.191.9} accept
            # api.nuget.org
            ip daddr {13.107.237.42,13.107.238.42} accept

            # DNS
            tcp dport 53 accept
            udp dport 53 accept

            # Accept traffic for root
            meta skuid 0 accept

            reject
          }

          chain forward {
            type filter hook forward priority 0;
            drop
          }
        }
      '';
    };

    systemd.services.nftables = {
      before = lib.mkForce [];
      after = lib.mkForce ["network-online.target"];
      wants = lib.mkForce ["network-online.target"];
      serviceConfig = {
        Restart = "on-failure";
      };
    };
  };
}
