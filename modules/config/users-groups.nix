{
  config,
  pkgs,
  lib,
  ...
}:
with lib; {
  options = {
    prologin.users.enable = mkEnableOption "Enable default users";

    # As services are submodules, this is a little trick to change the default
    # of an option of those submodules.
    security.pam.services = mkOption {
      type = with types;
        attrsOf (submodule {
          config = {
            makeHomeDir = mkDefault true;
          };
        });
    };
  };

  config = mkIf config.prologin.users.enable {
    security = {
      sudo.wheelNeedsPassword = false;
      # Currently, NixOS does not allow for adding extra stuff to pam. Here are
      # the relevant issues and merge requests:
      # https://github.com/NixOS/nixpkgs/issues/90640
      # https://github.com/NixOS/nixpkgs/issues/90488
      # https://github.com/NixOS/nixpkgs/pull/90490
      pam.services = {
        login.text =
          ''
            # Authentication management.
            auth  [default=ignore success=2]  pam_succeed_if.so                                         quiet uid <= 1001
            auth  sufficient                  ${pkgs.pam_krb5}/lib/security/pam_krb5.so                 minimum_uid=1000
          ''
          + (
            if config.prologin.afs.enable
            then ''
              auth  optional                    ${pkgs.pam_afs_session}/lib/security/pam_afs_session.so   program=${config.services.openafsClient.packages.programs}/bin/aklog nopag
            ''
            else ''
              auth  [default=ignore]            pam_deny.so
            ''
          )
          + ''
            auth  required                    pam_unix.so                                               try_first_pass nullok
            auth  optional                    pam_permit.so
            auth  required                    pam_env.so                                                conffile=/etc/pam/environment readenv=0

            # Account management.
            account   sufficient  ${pkgs.pam_krb5}/lib/security/pam_krb5.so
            account   required    pam_unix.so
            account   optional    pam_permit.so
            account   required    pam_time.so

            # Password management.
            password  sufficient  ${pkgs.pam_krb5}/lib/security/pam_krb5.so
            password  required    pam_unix.so                           try_first_pass nullok sha512 shadow
            password  optional    pam_permit.so

            # Session management.
            session   [default=ignore success=4]  pam_succeed_if.so                                         uid < 1000
            session   optional                    ${pkgs.pam}/lib/security/pam_mkhomedir.so                 silent skel=${config.security.pam.makeHomeDir.skelDirectory} umask=0077
            session   [default=ignore success=2]  pam_succeed_if.so                                         uid <= 1001
            session   required                    ${pkgs.pam_krb5}/lib/security/pam_krb5.so
          ''
          + (
            if config.prologin.afs.enable
            then ''
              session   required                    ${pkgs.pam_afs_session}/lib/security/pam_afs_session.so   afs_cells=${config.services.openafsClient.cellName} always_aklog minimum_uid=1000 program=${config.services.openafsClient.packages.programs}/bin/aklog nopag
            ''
            else ''
              session   [default=ignore]            pam_deny.so
            ''
          )
          + ''
            session   optional                    ${pkgs.systemd}/lib/security/pam_systemd.so
            session   required                    pam_unix.so
            session   optional                    pam_permit.so
            session   required                    pam_env.so                                                conffile=/etc/pam/environment readenv=0
            session   required                    pam_loginuid.so
          '';

        i3lock.text = config.security.pam.services.login.text;
        xfce4-screensaver.text = config.security.pam.services.login.text;
        sddm.text = config.security.pam.services.login.text;
        sshd.text = config.security.pam.services.login.text;
      };
    };

    prologin = {
      krb5.enable = mkDefault true;
      ldap.enable = mkDefault true;
    };

    # users override
    users = {
      mutableUsers = false;
      defaultUserShell = pkgs.bashInteractive;

      users = {
        prologin = {
          hashedPassword = "";
          isNormalUser = true;
          extraGroups = ["video" "audio"];
          uid = 1000;
        };
        root = {
          hashedPassword = lib.mkForce "$6$A7j7Nzw5mRVQ.SiW$s.OhjsTXExOwVrp93PTPQxNF.jkZ9YXiZKoy/rbhtPGARRsLfIMY44./5UqJIBBjms/AoOmBWvmmnEWG9thCb0";
          initialHashedPassword = lib.mkForce "$6$A7j7Nzw5mRVQ.SiW$s.OhjsTXExOwVrp93PTPQxNF.jkZ9YXiZKoy/rbhtPGARRsLfIMY44./5UqJIBBjms/AoOmBWvmmnEWG9thCb0";

          openssh.authorizedKeys.keys = lib.mkForce [
            # alarsyo
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIH3rrF3VSWI4n4cpguvlmLAaU3uftuX4AVV/39S/8GO9 alarsyo@thinkpad"
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMbf1C55Hgprm4Y7iNHae2UhZbLa6SNeurDTOyq2tr1G alarsyo@yubikey"
            # aurelien
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJSe8oZmEq2sQeAxLSd6l9EeUYRFT8tAB82E7+VQMQXI aurelien"
            # elfikur
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzPZWyYyxs+igQkURYlMrOvvP+HQaE/xnVvSyj+p47GJ0tj0dJdInXm4X4gv9hy9XLqz6UC4A/KrdHE9AIlHgxaciz0BcTgI41/SNkYOEIwTU6XO/U/dmsdTkIEkKcd3+6MX6HkkdO8ZFpBuk9/4vNKrgM+JUCgBoo9uo626h6EESmU+XSb6YgOc4UTzgr1HLtiSjOeF964zYQRHBLY/5sBk18A6TIRaKPi3Upl2Oe9/fzxW+oywiGpadZTUDtt+mDZwI+0oxwdRw6Fq6NNkIauaNuILCigj3Gjm8m/kldfLwWCNyIt322re9F9YRDR5uWjgCCcix9cpJX+i4bgVh3hbJ1XnoKHYOXrv1kNI2LoSL+q/7DUjo6MypXXFLS/g4Q+BdN8Pa0iU8fJiE0FabEFk4lZQym86hylRV+C4EYRWKHVNoNb2wBMSswgMkDaDAUyn4skXRdAUv0lU2H2qF4bHynmvA4XhGunxskKYsm4mIqg6K5T0Z5kc5g7wEAK0HPoBHXrcnlyd7nTUNT6fn1GfuVE7J31TX4nUTV+1XD3hXSTeNjpKjIMa108C+WTNFu7WdbDp9XZ1FsU92HsBaB5sDyodbzVDoUJCJv6mMPu4eTI5/43TTDpiKMGvLuJqciynYiljD3LDaiKa7hoQKeEpdNF+63+ETTVWaQVjjIcQ== elfikur"
            # idotno
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC7C5139FMUuus4YJacb+8QAZYzVDi0UmYLOtWcBDVpv3CHPaub2b6PB1bmWLVrqhWJkDGZfJkctXdvJmNz5xbdmTx+rsJqqE4LSY4UQ/g/XssOYK8K46VSHv/+9UDTgSzaJBMxf00v3oU4FsIi9R7p7GmoHYNuFHTRgzpb0kdZfbLnrbpDnt5iIMZJynQibFZKauawiEEQJU2tMBlBa3BaFDV061ECaAvE83c/ajJp87QCB3HVkOqwfB1qoGiGTYZmJ7WFeS8b7LMJzxUFd33yTTdxekrLw9ZZ5vw0/py5G4vXkoZjIDEq9iVImhMha2pTu8Tf7PXDBFQTTWOWrymWPG/yoSA4Duq2mgggEt3V953d47jKdVz6tkf+2Slkt8soh8ode7Wvla3vhgrJvlMdjyhJr4qHFLGkkZxF11rwDv0SWhZ1YnJj59LNqwJYMApm+A9hi21QvVNCMQUH/q96TlRbrW0i5IkgnOgIBKM1f/V7eGe3d/DzhsU2zDkdCzlKto05xH3eCShpsJzQBv23vBi9AyDDOMvw22bIPLbUnwsXdbVqKLyR1up4oVEiDzbXdbskrJDqp8/o48OssFAr+Rk3bl8/FDciB2Vfb/fP+cLNneSrs9udD5hMju0hakT74lYbaCh7NB6qPqq7AK8vCL5Oz6PjQ3QW6nwW+fzzKQ== idotno"
            # nhqml
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICxmhqikvC55CX3rcsJBbPf0DK+VTFLSmhtFG5bGXxPi nhqml"
            # risson
            "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIL0pnnKrvi9lrliSm+pf9HNAzs0GYLKiJk5AtSg4hhDq risson"
            # rookeur
            "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDCanbPYZGJPlXAbaUkVZPZbGMngjKctmm4dmBOv+eqAkNioeAtUKfmsNWJRZRqB+N2CcIn+0XtgIILNRS1/YC94oAkrSq7lXQ/Pb1vx/N8xQ+c8HCI+YVilKP3digpuVRa8eeCJLnOoBw0MKaXIvul6bG4rUSV+xxXMfeDb0BN9h1CLAnsdK3Ku0DIX57GqtPYbeqTN3tYZ3l+9uewAqUKJ28jEw+DTLUDLytyAw2XDalWMSlnC+WyoXMmwuYz74yKwiRzDPvOloDQf4cEZcdlywcz6HePTcH0d1oPaiTdDtIsI0eSSvZWSLBKA3hc+J3flH2Xyoz4Y7O5pdjoKK7dLoITHGK1Ue9MMCU8n9cqBFozDY7QtJuiY4bx9bdnhUc5PAXGpPR7vWM4tckEcaP8pEhED+h/u7TD4SyZlmUUZVyv7icrcCSQEbIKVLYi8bfMgXxDwkEiAOnTLCEDXT27VauSfQab68RmiMJvylW8ynl7rrM/3T0pbuzRXKJjGnuvNTNMQZqPwt+yp2BtXB/XdLEkh18PI35j/As+hhubxhtYW+esvuDw4a/JBWUtwO7MacM9VlKOtyV+LWKUljVwpgR5mCljauLnL5LulxmjRrcnm2nFCW7WzeSrilEV4AZ2lA0JJLMVI5VAWc6sP+LyM9TA3rmoaMxTxlJkni6Aw== rookeur"
          ];
        };
        staff = {
          hashedPassword = "$6$EnP3CVuLD6s6yYqk$QW/sKZ.12UCMZUuP5DwyHu.uOUecuIZMhVMOkmCNgNSRpR2vV.5BPxl22QOsZCSiKZWH8An87eCKpKZtnQj3k0";
          initialHashedPassword = "$6$EnP3CVuLD6s6yYqk$QW/sKZ.12UCMZUuP5DwyHu.uOUecuIZMhVMOkmCNgNSRpR2vV.5BPxl22QOsZCSiKZWH8An87eCKpKZtnQj3k0";
          isNormalUser = true;
          extraGroups = ["video" "audio" "network" "wheel"];
          uid = 1001;
        };
      };

      groups.network = {};
    };

    # Kill all user processes when logging out
    services.logind.killUserProcesses = true;

    security.pam.makeHomeDir.skelDirectory = toString ./skel;
  };
}
