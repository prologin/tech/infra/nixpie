{
  firewall = ./config/firewall.nix;
  krb5 = ./config/krb5.nix;
  ldap = ./config/ldap.nix;
  users-groups = ./config/users-groups.nix;
  afs = ./services/network-filesystems/openafs/client.nix;
  tailscale = ./services/tailscale.nix;
}
