{
  config,
  lib,
  pkgs,
  ...
}: let
  cfg = config.prologin.tailscale;
in {
  options = {
    prologin.tailscale = {
      enable = lib.mkEnableOption "Connect to a tailscale network on boot";
      loginServerUrl = lib.mkOption {
        type = lib.types.str;
        default = "https://vpn.prologin.org";
        example = "https://controlplane.tailscale.com";
        description = "URL of the login server to connect to";
      };
    };
  };

  config = lib.mkIf config.prologin.tailscale.enable {
    services.tailscale.enable = true;
    networking.firewall.checkReversePath = "loose";

    systemd.services.tailscale-login = {
      wantedBy = ["multi-user.target"];
      after = ["tailscaled.service" "network-online.target"];
      script = let
        authkeyStoreFile = ../../secrets/tailscale-authkey;
      in ''
        # make sure tailscaled has finished initializing otherwise the logged in
        # check below might fail even though we are logged in
        sleep 5

        echo "Checking if already authenticated to Tailscale ..."
        status="$(${config.services.tailscale.package}/bin/tailscale status -json | ${pkgs.jq}/bin/jq -r .BackendState)"
        if [ "$status" = "Running" ]; then  # do nothing
          echo "Already authenticated to Tailscale, exiting."
          sleep 5
          ${config.systemd.package}/bin/systemctl restart --no-block sssd.service
          exit 0
        fi

        if [ -s ${authkeyStoreFile} ]; then
          authKey="$(cat ${authkeyStoreFile})"
        else
          authKey="$(cat /proc/cmdline | ${pkgs.gnused}/bin/sed -n 's/.*ts_authkey=\([^ ]*\).*/\1/p')"
        fi

        if [ -z "$authKey" ]; then
          echo "No authkey provided, skipping tailscale login..."
          exit 1
        fi

        echo "Logging in..."
        ${config.services.tailscale.package}/bin/tailscale up --timeout 10s --login-server="${cfg.loginServerUrl}" --authkey="''${authKey}"
        sleep 30

        ${config.systemd.package}/bin/systemctl restart --no-block sssd.service
      '';
      serviceConfig = {
        Type = "oneshot";
        Restart = "on-failure";
        RemainAfterExit = true;
        ExecStop = pkgs.writeShellScript "tailscale-login-stop.sh" ''
          ${pkgs.coreutils}/bin/echo "Sending logout to tailscale control plane..."
          ${config.services.tailscale.package}/bin/tailscale logout

          # leave some room for logout to propagate to tailscale daemon then to control plane
          sleep 2
          ${pkgs.coreutils}/bin/echo "Logged out ephemeral node"
        '';
      };
    };
  };
}
