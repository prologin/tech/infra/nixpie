{
  config,
  pkgs,
  lib,
  inputs,
  ...
}:
with lib; {
  options = {
    prologin.afs = {
      enable = mkEnableOption "Enable default users";
    };
  };

  config = mkIf config.prologin.afs.enable {
    prologin.krb5.enable = true;

    services.openafsClient = {
      enable = true;
      cellName = "prologin.org";
      cellServDB = [
        {
          ip = "10.223.7.240";
          dnsname = "afs-0.pie.prologin.org";
        }
      ];
      cache = {
        diskless = true;
      };
      fakestat = true;
    };
  };
}
