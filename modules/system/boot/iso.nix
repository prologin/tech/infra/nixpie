{
  config,
  imageName,
  lib,
  modulesPath,
  pkgs,
  ...
}: {
  imports = ["${modulesPath}/installer/cd-dvd/iso-image.nix"];

  options = {
    iso = {
      freeSpaceMiB = lib.mkOption {
        type = lib.types.int;
        default = 10;
        description = "Free space to allocate for the persistent home partition";
      };
    };
  };

  config = {
    fileSystems = {
      "/home" = {
        fsType = "ext4";
        label = "rwhome";
      };
    };

    isoImage = {
      # Uncomment to speedup squashfs build, at the expense of a bigger ISO
      #squashfsCompression = "gzip -Xcompression-level 1";

      isoBaseName = imageName;
      makeUsbBootable = true;
      makeEfiBootable = true;
    };

    system.build.persistentIsoImage = pkgs.callPackage ../../../lib/make-persistent-iso.nix {
      isoImage = config.system.build.isoImage;
      partLabel = "rwhome";
      writableSpace = config.iso.freeSpaceMiB;
    };
  };
}
