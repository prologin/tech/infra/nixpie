{
  self,
  nixpkgs,
  ...
} @ inputs: channels: let
  inherit (nixpkgs) lib;

  mkDockerImage = image: config:
    channels.nixpkgs.dockerTools.buildLayeredImage {
      name = image;
      contents = config.system.path;
      extraCommands = ''
        mkdir -p /tmp
      '';
      config = {
        # See profiles/core/default.nix and modules/packages/default.nix
        Env = [
          "NIX_CFLAGS_COMPILE_x86_64_unknown_linux_gnu=-I/include"
          "NIX_CFLAGS_LINK_x86_64_unknown_linux_gnu=-L/lib"
          "PKG_CONFIG_PATH=/lib/pkgconfig"
        ];
      };
    };

  shouldBuildDockerImage = name: _: !lib.any (suffix: lib.hasSuffix suffix name) ["-vm" "-iso" "-netboot"];
in
  (lib.mapAttrs' (name: build: lib.nameValuePair "${name}-docker" (mkDockerImage name build.config)) (lib.filterAttrs shouldBuildDockerImage self.nixosConfigurations))
  // {
    nix-docker = channels.nixpkgs.docker-nixpkgs.nix.override {
      nix = channels.nixpkgs.nixFlakes;
      extraContents = [
        channels.nixpkgs.findutils
        (channels.nixpkgs.writeTextFile {
          name = "nix.conf";
          destination = "/etc/nix/nix.conf";
          text = ''
            experimental-features = experimental-features = nix-command flakes
            substituters = https://nix-cache.s3.prologin.org/ https://s3.cri.epita.fr/cri-nix-cache.s3.cri.epita.fr/ https://cache.nixos.org/
            trusted-public-keys = cache.nix.prologin.org:OZMs46jaN4mLZ3Tbb1oFC4JRuyNYZy29I+JoDKjLrh0= cache.nix.cri.epita.fr:qDIfJpZWGBWaGXKO3wZL1zmC+DikhMwFRO4RVE6VVeo= cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY=
          '';
        })
      ];
    };
  }
