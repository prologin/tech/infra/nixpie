{
  self,
  nixpkgs,
  ...
} @ inputs: let
  inherit (nixpkgs) lib;
  system = "x86_64-linux";

  nixosSystem = imageName: {extraModules ? []} @ args: let
    specialArgs = (import ./special-args.nix inputs imageName) // {inherit system;};

    modules = let
      inherit (import ./modules.nix inputs imageName) core global criNixpie flakeModules;
      local = import "${toString ./.}/${imageName}.nix";
    in
      flakeModules
      ++ criNixpie
      ++ [core global local]
      ++ extraModules;
  in rec {
    inherit system specialArgs modules;
  };

  allBootOptions = imageName: {extraModules ? []} @ args: {
    "${imageName}" = nixosSystem imageName args;
    "${imageName}-epita" = nixosSystem imageName {
      extraModules =
        extraModules
        ++ [
          ({
            config,
            lib,
            ...
          }: {
            system.build.toplevel-epita = config.system.build.toplevel-netboot;
            netboot.enable = true;
            prologin.afs.enable = lib.mkDefault true;
          })
        ];
    };
    "${imageName}-iso" = nixosSystem imageName {
      extraModules =
        extraModules
        ++ [
          ../modules/system/boot/iso.nix

          ({
            config,
            pkgs,
            ...
          }: {
            environment.systemPackages = [pkgs.wpa_supplicant_gui];
            networking.wireless = {
              enable = true;
              userControlled = {
                enable = true;
                group = "network";
              };
            };

            systemd.services.resize-home = {
              wantedBy = ["multi-user.target"];
              serviceConfig.Type = "oneshot";
              script = ''
                # Resize main partition to fill the whole disk
                device="$(${pkgs.coreutils}/bin/readlink -f /dev/disk/by-label/rwhome | ${pkgs.gnused}/bin/sed 's/[0-9]\+$//')"
                echo ", +" | ${pkgs.utillinux}/bin/sfdisk "$device" --no-reread -N 3
                ${pkgs.parted}/bin/partprobe || true
                ${pkgs.e2fsprogs}/bin/resize2fs /dev/disk/by-label/rwhome
              '';
            };
          })
        ];
    };
    "${imageName}-ova" = nixosSystem imageName {
      extraModules =
        extraModules
        ++ [
          ({modulesPath, ...}: {
            imports = [
              "${modulesPath}/virtualisation/virtualbox-image.nix"
            ];

            virtualbox = {
              vmName = imageName;
              vmFileName = "${imageName}.ova";
            };
          })
        ];
    };
  };

  images = {
    "concours-er" = {};

    "nixos-exec" = {};
    # used to test things like LiveISOs
    "nixos-test" = {};
  };
in
  lib.concatMapAttrs allBootOptions images
