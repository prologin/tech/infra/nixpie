{
  config,
  lib,
  pkgs,
  inputs,
  ...
}: {
  imports = [
    "${inputs.nixpie}/profiles/graphical"
  ];

  cri = {
    idle-shutdown.enable = lib.mkForce false;
    packages.pkgs.desktop.enable = lib.mkForce false;
    sddm = {
      title = "Prologin - ER";
    };
    xfce.enable = true;
    i3.enable = lib.mkForce false;
  };

  prologin = {
    firewall.enable = true;
  };

  environment.variables = {
    PYCHARM_PYTHON_PATH = "${pkgs.python310}/bin/python3.10";
    DOTNET_ROOT = "${pkgs.dotnet-sdk}";
  };

  environment.systemPackages = with pkgs;
    [
      (wrapFirefox firefox-unwrapped {
        extraPrefs = ''
          pref("network.negotiate-auth.trusted-uris", ".prologin.org");
          pref("network.trr.excluded-domains", "prologin.org");
        '';
      })
      xfce.xfce4-xkb-plugin
    ]
    # programming languages
    ++ [
      # ada
      gnat

      # c, c++
      (hiPrio gcc)
      # qtcreator needs those
      cmake
      libsForQt5.qt5.qtbase

      # c#
      msbuild
      dotnet-sdk

      # C#
      mono

      # D
      dmd

      go_1_18

      # haskell
      ghc

      # java build systems
      maven
      gradle

      # javascript
      nodejs

      lua

      ocaml
      ocamlPackages.merlin
      ocamlPackages.utop

      # pascal
      fpc

      perl

      php

      swiPrologWithGui

      python310

      ruby_3_0

      rust-analyzer
      rustc
      cargo

      # scheme
      gambit
    ]
    ++ [
      # a complete emacs configuration for every supported language is probably
      # out of scope, but we're getting a lot of angry OCaml users so let's
      # provide them with a decent environment inside their favorite OS.
      ((emacsPackagesFor emacs).emacsWithPackages (epkgs: (with epkgs.melpaPackages; let
        defaultEmacsConfig = pkgs.writeText "default.el" ''
          ;; utop default setup
          (autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
          (add-hook 'tuareg-mode-hook 'utop-minor-mode)

          ;; merlin default setup
          (add-hook 'tuareg-mode-hook #'merlin-mode)
          (add-hook 'caml-mode-hook #'merlin-mode)
          (with-eval-after-load 'company
            (add-to-list 'company-backends 'merlin-company-backend))
          ;; Enable company on merlin managed buffers
          (add-hook 'merlin-mode-hook 'company-mode)
        '';
      in [
        (runCommand "default.el" {} ''
          mkdir -p $out/share/emacs/site-lisp
          cp ${defaultEmacsConfig} $out/share/emacs/site-lisp/default.el
        '')
        # completion
        company
        # ocaml
        merlin
        merlin-company
        tuareg
        utop
        # rust
        flycheck-rust
        lsp-mode
        rust-mode
        rustic
      ])))

      (vscode-with-extensions.override {
        vscodeExtensions = with vscode-extensions; [
          ms-dotnettools.csharp
          ms-python.python
          ms-python.vscode-pylance
          ms-vscode.cpptools
          ocamllabs.ocaml-platform
          rust-lang.rust-analyzer
          vscodevim.vim
        ];
      })

      atom
      codeblocks
      eclipses.eclipse-java
      geany
      gnome.gedit
      jetbrains.idea-community
      jetbrains.pycharm-community
      leafpad
      libsForQt5.kate
      mg
      nano
      neovim
      netbeans
      qtcreator
      thonny
    ];

  programs.java = {
    enable = true;
    package = pkgs.jdk8;
  };
}
