{
  config,
  pkgs,
  lib,
  inputs,
  ...
}: {
  imports = [
    "${inputs.nixpie}/images/nixos-exec.nix"
  ];

  environment.variables = {
    NFS_SERVER = "clone-store.pie.prologin.org";
  };

  prologin = {
    afs.enable = false;
    krb5.enable = false;
    ldap.enable = false;
    tailscale.enable = lib.mkForce false;
  };
}
