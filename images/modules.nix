{
  self,
  nixpkgs,
  ...
} @ inputs: imageName: let
  inherit (nixpkgs) lib;
in rec {
  core = self.nixosModules.profiles.core;

  global = {
    system.name = imageName;
    networking.hostName = lib.mkForce ""; # Use the DHCP provided hostname
    nix.generateNixPathFromInputs = true;
    nix.generateRegistryFromInputs = true;
    nix.linkInputs = true;

    environment.etc."nixos-version".text =
      if (self ? rev)
      then self.rev
      else "";
    system.configurationRevision = lib.mkForce null; # triggers rebuild of mandb
  };

  labelOverride = let
    mkFlakeVersion = flake: "${lib.substring 0 8 (flake.lastModifiedDate or flake.lastModified or "19700101")}-${flake.shortRev or "dirty"}";
  in {
    system.nixos.versions = lib.mkForce (lib.mapAttrs (_: flake: mkFlakeVersion flake) {
      nixpie = self;
      criNixpie = inputs.nixpie;
      inherit
        (inputs)
        nixpkgs
        ;
    });
  };

  criNixpie = [
    inputs.nixpie.nixosModules.afs
    inputs.nixpie.nixosModules.aria2
    inputs.nixpie.nixosModules.audio
    inputs.nixpie.nixosModules.bluetooth
    inputs.nixpie.nixosModules.i3
    inputs.nixpie.nixosModules.idle-shutdown
    inputs.nixpie.nixosModules.krb5
    inputs.nixpie.nixosModules.label
    labelOverride
    inputs.nixpie.nixosModules.ldap
    inputs.nixpie.nixosModules.netboot
    inputs.nixpie.nixosModules.node-exporter
    inputs.nixpie.nixosModules.packages
    inputs.nixpie.nixosModules.redshift
    inputs.nixpie.nixosModules.salt
    inputs.nixpie.nixosModules.sddm
    inputs.nixpie.nixosModules.sshd
    inputs.nixpie.nixosModules.users
    inputs.nixpie.nixosModules.xfce
    inputs.nixpie.nixosModules.yubikey
  ];

  flakeModules =
    builtins.attrValues (removeAttrs self.nixosModules ["profiles" "nixpie"]);
}
